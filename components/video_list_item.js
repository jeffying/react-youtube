import React, { component } from 'react';

const VideoListItem = (props) => {
    const video = props.video;
    const onVideoSelect = props.onVideoSelect;
    //console.log(video);

    const imageUrl = video.snippet.thumbnails.default.url;
    const title = video.snippet.title;

    return (
        <li onClick={() => onVideoSelect(video) } className="list-group-item">
            <div className="video-list media">
                <div className="media-left">
                    <img src={imageUrl} alt="" className="media-object"/>
                </div>

                <div className="media-body">
                    <div className="media-heading">{title}</div>
                </div>

            </div>

        </li>
    );
}

export default VideoListItem;