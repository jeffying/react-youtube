import React, { Component } from 'react'

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {term: ''};
    }

    render() {
        return (
            <div className='col-md-12'>
                <input 
                    id='txt-search' 
                    value={this.state.term}
                    onChange={event => this.onInputChange(event.target.value)}
                    placeholder="Enter search term"
                />
            </div>

        );

    }
    
    onInputChange(term) {
        this.setState({term});
        this.props.onSearchTermChange(term);
    }



    // onInputChange(event) {
    //     console.log(event.target.value);
    // }
}


export default SearchBar;