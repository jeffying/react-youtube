import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import Config from './config';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';


import YTSearch from 'youtube-api-search';

const API_KEY = Config.keys.youtube;

class App extends Component {
    constructor(props){
        super(props);
        
        this.state = { 
            videos: [],
            selectedVideo: null,
            term: 'null'
         };

         this.videoSearch('hearthstone');
    }

    videoSearch(term) {
        YTSearch({key: API_KEY, term: term}, (videos) => {
           // console.log(videos);
            this.setState({ 
                videos: videos,
                selectedVideo: videos[0]
             });
        });
    }

    render() {
        const videoSearch = _.debounce((term) => { this.videoSearch(term)}, 300);
        return (
            <div>
                <h1>Youtube API search with React!</h1>
                <SearchBar onSearchTermChange={videoSearch} />
                <VideoDetail video={this.state.selectedVideo} />
                <VideoList 
                onVideoSelect={selectedVideo => this.setState({selectedVideo}) }
                videos={this.state.videos} />
                
            </div>
        );
    }
}


// Render the app to the page
ReactDOM.render(<App />, document.querySelector('.container'));
